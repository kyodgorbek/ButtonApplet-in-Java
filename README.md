# ButtonApplet-in-Java




import java.applet.Applet;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ButtonApplet extends Applet
{
  public ButtonApplet()
  {
      // the rectangle that the paint method draws
      box = new Rectangle(BOX_X, BOX_Y, BOX_WIDTH, BOX_HEIGHT);
      
      // the text fields for entering the x- and y- coordinates
      final JTextField xField = new JTextField(5);
      final JTextField yField = new JTextField(5);
     
     // the button to move the rectangle
     JButton moveButton = new JButton("Move", new ImageIcon("hand.gif"));
     
     class MoveButtonListener implements ActionListener
     {
       public void actionPerformed(ActionEvent event)
       {
          int x = Integer.parseInt(xField.getText());
          int y = Integer.parseInt(yField.getText());
          box.setLocation(x, y);
          repaint();
       }
     };
    
    ActionListener listener = new MoveButtonListener();
    moveButton.addActionListener(listener);
    
    // the labels for labeling the text fields
    JLabel xLabel = new JLabel("x = ");
    JLabel yLabel = new JLabel("y = ");
    
    // the panel for holding the user interface components
    JPanel panel = new JPanel();
    
    panel.add(xLabel);
    panel.add(xField);
    panel.add(yLabel);
    panel.add(yField);
    panel.add(moveButton);
    
    // the frame for holding the component panel
    JFrame frame = new JFrame();
    frame.setContentPane(panel);
    frame.pack();
    frame.show();
  }   
          
  public void paint (Graphics g)
  {
      Graphics2D g2 = (Graphics2D)g;
      g2.draw(box);
  }     
   
  private Rectangle box;
  private static final int BOX_X = 100;
  private static final int BOX_Y = 100;
  private static final int BOX_WIDTH = 20;
  private static final int BOX_HEIGHT = 30;
 }      
    
